const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const tailwindcss = require('tailwindcss');
const autoprefixer = require('autoprefixer');

const BUILD_DIR = path.resolve(__dirname, 'build/');
const SOURCE_DIR = path.resolve(__dirname, 'src/');

module.exports = {
  entry: `${SOURCE_DIR}/index.jsx`,
  output: {
    path: BUILD_DIR,
    filename: 'bundle,js',
  },
  resolve: {
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [{
      test: /\.(js|jsx)$/,
      use: ['babel-loader'],
    }, {
      test: /\.css$/,
      use: [
        'style-loader',
        {
          loader: 'postcss-loader',
          options: {
            ident: 'postcss',
            plugins: [
              tailwindcss,
              autoprefixer,
            ],
          },
        },
      ],
    }, {
      test: /\.(png|jpg|gif)$/,
      use: [{
        loader: 'url-loader',
        options: {
          limit: 8192,
        },
      }],
    }, {
      test: /\.svg$/,
      use: [
        {
          loader: 'babel-loader',
        },
        {
          loader: 'react-svg-loader',
          options: {
            jsx: true,
          },
        },
      ],
    }],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: `${SOURCE_DIR}/index.html`,
    }),
    new CleanWebpackPlugin(),
  ],
};
