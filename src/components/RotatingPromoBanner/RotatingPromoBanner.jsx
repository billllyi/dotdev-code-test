import React from 'react';
import tw from 'tailwind.macro';
import styled from 'styled-components';

import CloseImg from '../../assets/close.svg';

const Banner = styled.div`
  ${tw`relative bg-blue text-center overflow-hidden`};
  transition: all .4s ease-out;
  visibility: ${props => (props.isClose ? 'hidden' : '')};
  opacity: ${props => (props.isClose ? '0' : '')};
`;

const Content = styled.div`
  ${tw`py-2.5 leading-none text-sm font-medium font-sofia`};
`;

const Span = styled.span`
  ${tw`text-white`};
`;

const PinkSpan = styled(Span)`
  ${tw`text-pink`};
`;

const Button = styled.button`
  ${tw`absolute `}
  right: 6px;
  top: 50%;
  transform: translateY(-50%);
`;

const Img = styled(CloseImg)`
  transform: rotate(45deg);
`;

class RotatingPromoBanner extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isClose: false,
    };
    this.onCloseClick = this.onCloseClick.bind(this);
  }

  onCloseClick() {
    this.setState({ isClose: true });
  }

  render() {
    const { isClose } = this.state;
    return (
      <Banner isClose={isClose}>
        <Content>
          <PinkSpan>Free Shipping </PinkSpan>
          <Span>on all Australian orders over </Span>
          <PinkSpan>$80</PinkSpan>
        </Content>
        <Button onClick={this.onCloseClick}>
          <Img />
        </Button>
      </Banner>
    );
  }
}

export default RotatingPromoBanner;
