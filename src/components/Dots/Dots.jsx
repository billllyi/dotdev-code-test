import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const StyledDots = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  width: 5.25rem;
  justify-content: space-between;
`;

const Dot = styled.div`
  border: 0.125rem solid #4d4a47;
  border-radius: 50%;
`;

const Dots = ({ className }) => (
  <StyledDots className={className}>
    <Dot />
    <Dot />
    <Dot />
    <Dot />
    <Dot />
  </StyledDots>
);

Dots.propTypes = {
  className: PropTypes.string.isRequired,
};

export default React.memo(Dots);
