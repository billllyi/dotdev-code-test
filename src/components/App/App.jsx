import React from 'react';
import styled from 'styled-components';
import tw from 'tailwind.macro';

import RotatingPromoBanner from '../RotatingPromoBanner';
import CarouselBanner from '../CarouselBanner';
import ColumnImage from '../ColumnImage';
import ProductGrid from '../ProductGrid';
import HeroTextGrid from '../HeroTextGrid';

const ContentWrapper = styled.div`
  ${tw`w-3/4 mx-auto`}
`;

const App = () => (
  <React.Fragment>
    <RotatingPromoBanner />
    <ContentWrapper>
      <CarouselBanner />
      <br />
      <br />
      <ColumnImage />
      <br />
      <br />
      <ProductGrid />
      <br />
      <br />
      <HeroTextGrid />
      <br />
      <br />
    </ContentWrapper>
  </React.Fragment>
);

export default App;
