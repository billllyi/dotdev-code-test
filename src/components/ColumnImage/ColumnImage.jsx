import React from 'react';
import styled from 'styled-components';
import tw from 'tailwind.macro';

import luca from '../../assets/luca-shot-12-92314.jpg';
import rose from '../../assets/shot-5-rose-63040.jpg';
import boys from '../../assets/19-down-river-boys-1.jpg';

import ImageContainer from './ImageContainer';

const imagesList = [
  {
    id: 1,
    image: luca,
  }, {
    id: 2,
    image: rose,
  }, {
    id: 3,
    image: boys,
  },
];

const StyledColumnImage = styled.div`
  ${tw`w-11/12 mb-0 mx-auto flex flex-wrap items-center justify-around`}
`;

const ColumnImage = () => (
  <StyledColumnImage>
    {imagesList.map(({ image, id }) => (<ImageContainer key={id} image={image} />))}
  </StyledColumnImage>
);

export default React.memo(ColumnImage);
