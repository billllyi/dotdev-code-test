import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import tw from 'tailwind.macro';

import Dots from '../Dots';
import UnderlinedButton from '../UnderlinedButton';

const StyledImageContainer = styled.div`
  ${tw`sm:w-full lg:w-1/3 flex flex-col items-center`}
  margin-top: 2.625rem;
`;

const Image = styled.img`
  border: 0.5rem solid transparent;
  border-radius: 50%;
  max-height: 260px;
  :hover {
    ${tw`border-borderblue`}
    transition: all .5s ease-in-out;
  }
`;

const MarginedDots = styled(Dots)`
  ${tw`mt-8`}
`;

const Content = styled.div`
  ${tw`font-sofia text-2xl text-center text-darkgray mt-4`}
  line-height: 1.38;
`;

const Button = styled(UnderlinedButton)`
  ${tw`mt-4 p-1.25`}
`;

const ImageContainer = ({ image }) => (
  <StyledImageContainer>
    <Image src={image} alt="column photo" />
    <MarginedDots />
    <Content>Ready for Bed</Content>
    <Button>SHOP NOW</Button>
  </StyledImageContainer>
);

ImageContainer.propTypes = {
  image: PropTypes.string.isRequired,
};

export default React.memo(ImageContainer);
