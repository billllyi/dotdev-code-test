import styled from 'styled-components';
import tw from 'tailwind.macro';

const UnderlinedButton = styled.button`
  ${tw`font-sofia text-xs font-semibold text-center text-darkgray pb-1.5`}
  letter-spacing: 1px;
  border-bottom: 1px solid #1f1f1e;
`;

export default UnderlinedButton;
