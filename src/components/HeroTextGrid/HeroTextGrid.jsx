import React from 'react';
import styled from 'styled-components';
import tw from 'tailwind.macro';

import LeftBgImage from '../../assets/group-29.svg';
import RightBgImage from '../../assets/group-12.svg';

import UnderlinedButton from '../UnderlinedButton';

const StyledHeroTextGrid = styled.div`
  ${tw`w-11/12 my-0 mx-auto flex flex-col items-center relative`}
`;

const StyledLeftBgImage = styled(LeftBgImage)`
  ${tw`absolute left-0`}
`;

const StyledRightBgImage = styled(RightBgImage)`
  ${tw`absolute right-0 bottom-0`}
`;

const Title = styled.h1`
  ${tw`font-sofia text-2xl text-center text-darkgray`}
  line-height: 1.38;
`;

const Text = styled.div`
  ${tw`w-5/12 font-ebgaramond text-base text-center text-darkgray mt-3`}
  line-height: 1.44;
`;

const Button = styled(UnderlinedButton)`
  margin-top: 2.25rem;
`;

const HeroTextGrid = () => (
  <StyledHeroTextGrid>
    <StyledLeftBgImage />
    <StyledRightBgImage />
    <Title>Pure Baby Edit</Title>
    <Text>
      Newborn babies are beautiful little wonders—fragile and precious. Whether you’re
       dressing a boy or girl, it’s essential to choose the right baby clothes and accessories.
       From growsuits to beanies, shop our latest collection of styles and prints.
    </Text>
    <Button>SHOP NOW</Button>
  </StyledHeroTextGrid>
);

export default React.memo(HeroTextGrid);
