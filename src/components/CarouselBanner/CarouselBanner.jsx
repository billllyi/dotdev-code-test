import React from 'react';
import styled from 'styled-components';
import Slider from 'react-slick';
import tw from 'tailwind.macro';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import bannerImg from '../../assets/shot-8-valentina-1143.jpg';
import ArrowLeft from '../../assets/baseline-arrow_back-24px.svg';
import ArrowRight from '../../assets/baseline-arrow_forward-24px.svg';

import PromoCard from './PromoCard';

const StyledSlider = styled(Slider)`
  .slick-dots {
    position: absolute;
    right: 5.5rem;
    bottom: 1.25rem;
    width: 15%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: baseline;

    .slick-active > div {
      border-color: #cae0e8;
    }
  }
`;

const SliderWarpper = styled.div`
  ${tw`relative`}
`;

const ImgWrapper = styled.div`
  ${tw`relative my-0 mx-2`}
`;

const CustomizedDot = styled.div`
  border-width: ${props => (props.i + 1) * -0.35 + 5.3}px;
  border-radius: 50%;
  border-color: #fff;
  height: ${props => (props.i + 1) * -0.7 + 10.6}px;
  width: ${props => (props.i + 1) * -0.7 + 10.6}px;
`;

const ArrowButton = styled.div`
  ${tw`flex absolute bg-white items-center justify-center cursor-pointer`}
  top: 50%;
  transform: translateY(-50%);
  height: 2.25rem;
  width: 2.25rem;
  border-radius: 50%;
`;

const LeftArrowButton = styled(ArrowButton)`
  left: 2.25rem;
`;

const RightArrowButton = styled(ArrowButton)`
  right: 2.25rem;
`;

class CarouselBanner extends React.PureComponent {
  constructor(props) {
    super(props);
    this.sliderRef = (element) => {
      this.slider = element;
    };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.settings = {
      className: 'center',
      centerMode: true,
      centerPadding: '100px',
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      variableWidth: false,
      variableHeight: true,
      dots: true,
      customPaging: i => <CustomizedDot i={i} />,
      arrows: false,
    };
  }

  next() {
    this.slider.slickNext();
  }

  previous() {
    this.slider.slickPrev();
  }

  render() {
    return (
      <SliderWarpper>
        <StyledSlider ref={this.sliderRef} {...this.settings}>
          <ImgWrapper>
            <img src={bannerImg} alt="banner" />
            <PromoCard />
          </ImgWrapper>
          <ImgWrapper>
            <img src={bannerImg} alt="banner" />
          </ImgWrapper>
          <ImgWrapper>
            <img src={bannerImg} alt="banner" />
          </ImgWrapper>
          <ImgWrapper>
            <img src={bannerImg} alt="banner" />
          </ImgWrapper>
          <ImgWrapper>
            <img src={bannerImg} alt="banner" />
          </ImgWrapper>
        </StyledSlider>
        <LeftArrowButton onClick={this.previous}>
          <ArrowLeft />
        </LeftArrowButton>
        <RightArrowButton onClick={this.next}>
          <ArrowRight />
        </RightArrowButton>
      </SliderWarpper>
    );
  }
}

export default CarouselBanner;
