import React from 'react';
import styled from 'styled-components';
import tw from 'tailwind.macro';

import Image from '../../assets/group-10.svg';

import Dots from '../Dots';

const CardWrapper = styled.div`
  ${tw`bg-white absolute flex items-center flex-col`}
  height: 23.75rem;
  width: 19.375rem;
  border-radius: 0.75rem;
  right: 3.8125rem;
  top: 50%;
  transform: translateY(-50%);
`;

const BackgroundImg = styled(Image)`
  ${tw`absolute`}
  top: 1.5rem;
  left: 1rem;
`;

const FirstDots = styled(Dots)`
  margin-top: 2.5rem;
`;

const Header = styled.div`
  ${tw`text-xs font-sofia font-semibold items-center text-darkgray mt-4`}
  letter-spacing: 1px;
`;

const Content = styled.div`
  ${tw`font-router font-medium italic text-center leading-none text-red mt-12`}
  font-size: 2.8125rem;
`;

const ExtraContent = styled.div`
  ${tw`font-sofia text-lg text-center text-darkgray`}
  line-height: 1.83;
  margin-top: 0.375rem;
`;

const SecondDots = styled(Dots)`
  margin-top:1.5625rem;
`;

const Instructions = styled.div`
  ${tw`flex flex-row items-start mt-4`}
  margin-bottom: 0.6875rem;
`;

const Instruction = styled.div`
  ${tw`font-sofia text-xs font-semibold leading-none text-center text-darkgray inline-block`}
  letter-spacing: 1px;
`;

const UnderlinedInstruction = styled(Instruction)`
  border-bottom: 1px solid #4d4a47;
  padding-bottom: 0.3125rem;
`;

const Restriction = styled.div`
  ${tw`font-ebgaramond text-center text-darkgray`}
  font-size: 0.625rem;
  line-height: 1.4;
`;

const PromoCard = () => (
  <CardWrapper>
    <BackgroundImg />
    <FirstDots />
    <Header>CLICK FRENZY STARTS NOW</Header>
    <Content>25% off Storewide</Content>
    <ExtraContent>includes sale*</ExtraContent>
    <SecondDots />
    <Instructions>
      <Instruction>USE CODE: </Instruction>
      <UnderlinedInstruction>PBC25</UnderlinedInstruction>
      <Instruction> AT CHECKOUT</Instruction>
    </Instructions>
    <Restriction>
      *Online only. Excludes Essentials.
    </Restriction>
    <Restriction>
      Ends 11.59pm Wednesday 22 May
    </Restriction>
  </CardWrapper>
);

export default React.memo(PromoCard);
