import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import tw from 'tailwind.macro';

import LikeImage from '../../assets/resting.svg';

const StyledProduct = styled.div`
  ${tw`flex flex-col items-center relative sm:w-1/2 lg:w-1/3 xl:w-1/6`}
`;

const Image = styled.img`
  ${tw`my-0 mx-4`}
  height: 12.25rem;
`;

const Highlight = styled.div`
  ${tw`absolute rounded bg-highlightbgblue font-sofia font-semibold text-center text-darkgray py-2.25 px-4`}
  left: 1.5rem;
  top: 0.5rem;
  font-size: 9px;
  line-height: 1.1;
  letter-spacing: 0.75px;
`;

const Name = styled.div`
  ${tw`font-sofia text-sm text-center text-lightblack mt-1.5`}
  line-height: 1.43;
`;

const Price = styled.div`
  ${tw`font-sofia text-sm text-center text-lightblack`}
  line-height: 1.43;
`;

const Like = styled.div`
  ${tw`mt-1.5 mb-0 my-auto`}
`;

const StyledLikeImage = styled(LikeImage)`
  path {
    fill: ${props => (props.fill ? 'red' : '')};
  }
`;

class Product extends React.PureComponent {
  constructor(props) {
    super(props);
    const { defaultIsLiked } = this.props;
    this.state = {
      isLiked: defaultIsLiked,
    };
    this.onLikeClick = this.onLikeClick.bind(this);
  }

  onLikeClick() {
    this.setState(prevState => ({
      isLiked: !prevState.isLiked,
    }));
  }

  render() {
    const {
      image,
      highlight,
      name,
      price,
    } = this.props;

    const { isLiked } = this.state;

    return (
      <StyledProduct>
        <Image src={image} alt={name} />
        {highlight && <Highlight>{highlight}</Highlight>}
        <Name>{name}</Name>
        <Price>{`$${price}`}</Price>
        <Like onClick={this.onLikeClick}>
          <StyledLikeImage fill={isLiked ? 1 : 0} />
        </Like>
      </StyledProduct>
    );
  }
}

Product.defaultProps = {
  highlight: null,
};

Product.propTypes = {
  image: PropTypes.string.isRequired,
  highlight: PropTypes.string,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  defaultIsLiked: PropTypes.bool.isRequired,
};

export default Product;
