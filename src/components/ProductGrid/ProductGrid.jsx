import React from 'react';
import styled from 'styled-components';
import tw from 'tailwind.macro';

import productImage from '../../assets/rectangle.jpg';

import Product from './Product';

const productList = [{
  id: 1,
  image: productImage,
  highlight: 'BEST SELLER',
  name: 'Swan Jumper',
  price: 59.95,
  isLiked: false,
}, {
  id: 2,
  image: productImage,
  highlight: 'BEST SELLER',
  name: 'Fishermans Cable Beanie',
  price: 19.95,
  isLiked: false,
}, {
  id: 3,
  image: productImage,
  highlight: 'BEST SELLER',
  name: 'Limted Fox Jumper',
  price: 59.95,
  isLiked: false,
}, {
  id: 4,
  image: productImage,
  highlight: 'BEST SELLER',
  name: 'Baby Dress',
  price: 59.95,
  isLiked: false,
}, {
  id: 5,
  image: productImage,
  highlight: 'BEST SELLER',
  name: 'Long Eyelet Cardigan',
  price: 59.95,
  isLiked: false,
}, {
  id: 6,
  image: productImage,
  name: 'Pond Jumper',
  price: 59.95,
  isLiked: false,
}];

const StyledProductGridWrapper = styled.div`
  ${tw`flex flex-col items-center`}
`;


const Title = styled.h1`
  ${tw`font-sofia text-lg text-center text-darkgray mt-1`}
  line-height: 1.83;
`;

const ProductGridWrapper = styled.div`
  ${tw`flex flex-wrap justify-around mt-5`}
`;

const ProductGrid = () => (
  <StyledProductGridWrapper>
    <Title>
      Wintor Wonders
    </Title>
    <ProductGridWrapper>
      {
        productList.map(
          ({ id, isLiked, ...props }) => (<Product key={id} defaultIsLiked={isLiked} {...props} />),
        )
      }
    </ProductGridWrapper>
  </StyledProductGridWrapper>
);

export default React.memo(ProductGrid);
