/* eslint-disable quote-props */
module.exports = {
  theme: {
    colors: {
      'pink': '#fddcda',
      'blue': '#153b60',
      'white': '#fff',
      'darkgray': '#1f1f1e',
      'red': '#ea2223',
      'underlineblack': '#4d4a47',
      'borderblue': '#76B7C1',
      'highlightbgblue': '#cae0e8',
      'lightblack': '#4d4a47',
    },
    fontFamily: {
      'sofia': ['sofia-pro'],
      'router': ['Router'],
      'ebgaramond': ['eb-garamond'],
    },
    extend: {
      spacing: {
        '0.5': '0.125rem', // 2px
        '0.75': '0.1875rem', // 3px
        '1.25': '0.3125rem', // 5px
        '1.5': '0.375rem', // 6px
        '1.75': '0.4375rem', // 7px
        '2.25': '0.5625rem', // 9px
        '2.5': '0.625rem', // 10px
        '2.75': '0.6875rem', // 11px
        '3.25': '0.8125rem', // 13px
        '3.5': '0.875rem', // 14px
        '3.75': '0.9375rem', // 15px
      },
    },
  },
  variants: {},
  plugins: [],
};
